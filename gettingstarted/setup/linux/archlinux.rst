.. _archlinux:

archlinux
###########


*************************************
Installing VLC on archlinux:
*************************************


**Download the up-to-date package from the official archlinux repository (*Recommended for beginners*):**

**Run:**

      .. code-block::

             pacman -S vlc


To run VLC using Web interface:
+++++++++++++++++++++++++++++++++++

* From the command line -

Run VLC with parameter

    .. code-block::

           vlc --extraintf=http --http-host 0.0.0.0 --http-port 8080 --http-password yourpasswordhere

Default Port:`8080 <http://localhost:8080/>`_

.. note:: To allow remote connections, edit :file:`/usr/share/vlc/lua/http/.hosts` and restart VLC.



To use Chromecast support:
+++++++++++++++++++++++++++++

To use Chromecast support, follow either of the following methods:

* Install:

1. `libmicrodns <https://www.archlinux.org/packages/?name=libmicrodns>`_ - To find the chromecast device listed in *Playback > Renderer menu*.
2. `protobuf <https://www.archlinux.org/packages/?name=protobuf>`_ - To enable streaming in the selected device in *Playback > Renderer menu*.

This might conflict with other software using mDNS/DNS-SD.

* Setup an Avahi server:

Avahi is a networking implementation used for multicast DNS/DNS-SD service discovery, which is often available by default in linux OS. In most Linux distributions, Avahi daemon is run with disallow-other-stack=no by default (secured mode).
The VLC module for avahi can be found `here <https://wiki.videolan.org/Documentation:Modules/bonjour/>`_.

Refer `Avahi: archlinux wiki <https://wiki.archlinux.org/index.php/Avahi>`_ for installation instructions.


Troubleshooting
+++++++++++++++++

If libmicrodns is installed in addition to Avahi daemon, in the default secured mode (with disallow-other-stack set to "no"), Chromecast support does not function.

To allow Chromecast support to function:

1. Run Avahi daemon in insecure mode with disallow-other-stack set to "yes".

2. Allow discovery of chromecast devices with the Avahi DNS-SD resolver.


Tips:
+++++++++


1. To install and apply a new skin in archlinux, make sure to store the downloaded skin in :file:`/.local/share/vlc/skins2/`

2. A list of FAQs and documented bugs can be found on the official Archlinux documentation:

- `Troubleshooting <https://wiki.archlinux.org/index.php/VLC_media_player#Troubleshooting>`_
- `Tricks <https://wiki.archlinux.org/index.php/VLC_media_player#Tips_and_tricks>`_
